[![pipeline status](https://gitlab.com/Timmy1e/kiririn-site/badges/master/pipeline.svg)](https://gitlab.com/Timmy1e/kiririn-site/commits/master)
[![coverage report](https://gitlab.com/Timmy1e/kiririn-site/badges/master/coverage.svg)](https://gitlab.com/Timmy1e/kiririn-site/commits/master)

# Kiririn.io website
This is the [Kiririn.io website](https://kiririn.io) NodeJS Docker project.

This is automatically built using the GitLab CI and pushed to the [Gitlab project Docker repo](https://gitlab.com/Timmy1e/kiririn-site/container_registry/1691838).
