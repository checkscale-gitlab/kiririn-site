/**
 * @copyright Kiririn.io
 * @since 2018
 * @author Tim van Leuverden <TvanLeuverden@Gmail.com>
 */
const path = require('path');
const redis = require('express-redis-cache');
const logger = require('morgan');
const express = require('express');
const favicon = require('serve-favicon');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const babelMiddleware = require('express-babel').default;


const app = express();
const PROD = process.env.NODE_ENV === 'production';


/// Redis cache
global.cache = redis({
	prefix: 'kiririn',
	host: process.env.REDIS_SERVICE_HOST,
	port: process.env.REDIS_SERVICE_PORT,
	expire: process.env.REDIS_EXPIRE ? parseInt(process.env.REDIS_EXPIRE) : 900
});


/// View engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


/// Middleware
app.use(favicon(path.join(__dirname, 'public', 'images', 'kiririn.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use('/scripts', babelMiddleware(path.join(__dirname, 'public', 'scripts'), {
	compact: true,
	minified: true,
	//blacklost: ['useStrict'],
	presets: ['env', 'minify'],
	plugins: ['transform-strict-mode']
}));
app.use(express.static(path.join(__dirname, 'public')));

// Dependencies
addStaticDep('angular');
addStaticDep('angular-animate');
addStaticDep('bootstrap', path.join('bootstrap', 'dist'));
addStaticDep('jquery', path.join('jquery', 'dist'));
addStaticDep('popper', path.join('popper.js', 'dist', 'umd'));
addStaticDep('moment', path.join('moment', 'min'));
addStaticDep('font-awesome');

function addStaticDep(depName, depPath = depName) {
	app.use('/dist/' + depName, express.static(path.join(__dirname, 'node_modules', depPath)));
}

app.get('/robots.txt', global.cache.route(), (req, res) => {
	res.type('text/plain');
	res.send(
		'User-agent: *\n' +
		'Allow: /'
	);
});

app.use('/', require('./routes/index'));
app.use('/kana', require('./routes/kana'));
app.use('/proxy', require('./routes/proxy'));
app.use('/import', require('./routes/import'));
app.use('/teletext', require('./routes/teletext'));
app.use('/tracking', require('./routes/tracking'));
app.use('/twitchPlaylist', require('./routes/twitchPlaylist'));

// catch 404 and forward to error handler
app.use((req, res, next) => {
	const err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use((err, req, res, next) => {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = PROD ? {} : err;

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;
