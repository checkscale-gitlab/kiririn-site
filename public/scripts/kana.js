/*global angular, $*/
/**
 * @copyright Kiririn.io
 * @since 2018
 * @author Tim van Leuverden <TvanLeuverden@Gmail.com>
 */

angular.module('kiririn').controller('kana', ['$scope', ($scope) => {

	$scope.currentSystem = '';
	$scope.currentModifier = '';
	$scope.kana = JSON.parse($('#kana').text());

	function init() {
		$scope.currentSystem = 'hiragana';
		$scope.currentModifier = 'standard';
	}

	$scope.switchSystem = () => $scope.currentSystem = ($scope.currentSystem === 'hiragana' ? 'katakana' : 'hiragana');

	$scope.switchModifier = () => $scope.currentModifier = ($scope.currentModifier === 'standard' ? 'dakuten' : 'standard');

	init();
}]);