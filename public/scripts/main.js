/*global angular */
/**
 * @copyright Kiririn.io
 * @since 2018
 * @author Tim van Leuverden <TvanLeuverden@Gmail.com>
 */
angular.module('kiririn', ['ngAnimate']);

$.fn.goTo = () => {
	$('html, body').animate({
		scrollTop: $(this).offset().top + 'px'
	}, 'fast');
	return this;
};

$(() => {
	$('[data-toggle="tooltip"]').tooltip();
});