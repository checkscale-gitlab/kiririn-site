/**
 * @copyright Kiririn.io
 * @since 2018
 * @author Tim van Leuverden <TvanLeuverden@Gmail.com>
 */

$(() => {
	window.onhashchange = locationHashChanged;
	if (location.hash) {
		locationHashChanged();
	} else {
		getTeletext(100);
	}
});

function locationHashChanged() {
	getTeletext(location.hash.replace('#', ''));
}

function getTeletext(page) {
	$.get(
		'/proxy/teletext/' + page,
		(data, status) => {
			if (status === 'success') {
				let content = JSON.parse(data);
				$('#teletextField').empty().append(content.content);
			}
		}
	);
}
