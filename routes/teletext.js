const express = require('express');
const router = express.Router();


router.get('/', global.cache.route(), (req, res, next) => {
	res.render('teletext', {
		route: [
			{title: 'Teletext', location: '/teletext'}
		]
	});
});

module.exports = router;
