const express = require('express');
const countries = require('country-list');
const router = express.Router();


router.get('/', global.cache.route(), (req, res, next) => {
	let trackingCode = req.query.trackingCode;
	trackingCode = trackingCode ? trackingCode : null;

	let countryCode = req.query.countryCode;
	countryCode = countryCode ? countryCode : null;

	let postalCode = req.query.postalCode;
	postalCode = postalCode ? postalCode : null;

	res.render('tracking', {
		route: [
			{title: 'Tracking', location: '/tracking'}
		],
		trackingCode: trackingCode,
		countryCode: countryCode,
		postalCode: postalCode,
		countries: countries.getData()
	});
});

router.get('/package.html', global.cache.route(), (req, res, next) => {
	res.render('templates/package');
});

router.get('/trackingText.html', global.cache.route(), (req, res, next) => {
	res.render('templates/trackingText');
});

module.exports = router;
